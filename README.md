Royer Quentin L3
 #projet PHP Symfony
Install process :

 - install : `composer install`
 
 - run the server : `php bin/console server:start`

 - modify RIOT API KEY with a valid one  
        in `src/Infra/Rest/RestClient`  
        set `const API_KEY = <valid api key>` 

 #Features : 
 
  - Free champions in the Homepage
  - header + footer 
  - search bar for summoner 
  - account login 
  - register with ajax to require a valid summoner name
  - champion list with paginator 
  - user account dashboard
  - find summoner and see his stats
  
 