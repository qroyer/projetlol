<?php


namespace App\Infra\Rest;

use Symfony\Component\HttpClient\HttpClient;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\ArrayDenormalizer;
use Symfony\Component\Serializer\Normalizer\GetSetMethodNormalizer;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;
use Symfony\Contracts\HttpClient\HttpClientInterface;

abstract class RestClient
{
    const API_KEY = 'RGAPI-ed060fbd-3aeb-4467-a1d9-7af87ddcb25e'; //set a valid RIOT API KEY
    const BASE_URL_API = 'https://euw1.api.riotgames.com';
    /**
     * @var Serializer
     */
    protected $serializer;
    /**
     * @var HttpClientInterface
     */
    protected $httpClient;

    /**
     * RestClient constructor.
     */
    protected function __construct()
    {
        $this->httpClient = HttpClient::create();
        $this->serializer = new Serializer(
            [new ObjectNormalizer(), new ArrayDenormalizer(), new GetSetMethodNormalizer(), new ArrayDenormalizer()],
            [new JsonEncoder()]
        );
    }

    /**
     * @return string
     */
    public function getApiKey(): string
    {
        return self::API_KEY;
    }

    /**
     * @return string
     */
    public function getApiKeyAsUrlParam(): string
    {
        return 'api_key=' . $this->getApiKey();
    }

}
