<?php

namespace App\Infra\Rest;

use App\Infra\Dto\LeagueEntryDTO;
use App\Infra\Dto\MiniSeriesDTO;
use App\Infra\Dto\Summoner;
use Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\DecodingExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;
use App\Infra\Dto\SummonerInfo;

/**
 * Class ChampionRestClient
 *
 * @package App\Infra\Rest
 */
class SummonerRestClient extends RestClient
{
    const SUMMONER_URI_API = '/lol/summoner/v4/summoners/by-name/';
    const SUMMONER_ID_URI_API = '/lol/summoner/v4/summoners/';
    const SUMMONER_INFOS_BY_ID_URI_API = '/lol/league/v4/entries/by-summoner/';

    /**
     * @var string
     */
    private $url;
    /**
     * @var array
     */
    private $freeChampions;

    /**
     * ChampionRestClient constructor.
     */
    public function __construct()
    {
        parent::__construct();
    }


    /**
     * @param $name
     * @return Summoner
     * @throws ClientExceptionInterface
     * @throws DecodingExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws ServerExceptionInterface
     * @throws TransportExceptionInterface
     */
    public function getSummonerByName($name)
    {
        $response = $this->httpClient->request(
            'GET',
            self::BASE_URL_API . self::SUMMONER_URI_API . $name . '?' . $this->getApiKeyAsUrlParam()
        );

        if ($response->getStatusCode() != 200) {
            return null;
        }

        $summonerData = $response->toArray();

        return $this->createSummonerObject($summonerData);
    }

    public function getSummonerInfo($id)
    {
        $response = $this->httpClient->request(
            'GET',
            self::BASE_URL_API . self::SUMMONER_INFOS_BY_ID_URI_API . $id . '?' . $this->getApiKeyAsUrlParam()
        );

        if ($response->getStatusCode() != 200) {
            return null;
        }

        $summonerData = $response->toArray();

        return $this->createSummonerInfoObject($summonerData);
    }

    public function getSummonerById($id)
    {
        $response = $this->httpClient->request(
            'GET',
            self::BASE_URL_API . self::SUMMONER_ID_URI_API . $id . '?' . $this->getApiKeyAsUrlParam()
        );

        if ($response->getStatusCode() != 200) {
            return null;
        }

        $summonerData = $response->toArray();

        return $this->createSummonerObject($summonerData);
    }

    /**
     * @param $data
     * @return Summoner
     */
    public function createSummonerObject($data)
    {
        return new Summoner(
            $data['id'],
            $data['accountId'],
            $data['puuid'],
            $data['name'],
            $data['profileIconId'],
            $data['revisionDate'],
            $data['summonerLevel']
        );
    }

    public function createSummonerInfoObject($data)
    {

        $array1 = array_shift($data);
        $array2 = array_shift($data);

        $flex = null;
        $solo = null;

        if ($array1 and $array1['queueType'] =='RANKED_FLEX_SR') {
            $flex = $array1;
            $solo = $array2;
        } elseif ($array1 and $array1['queueType']=='RANKED_SOLO_5x5'){
            $flex = $array2;
            $solo = $array1;
        }

        if($flex){
                $flexObj = new LeagueEntryDTO(
                    $flex['leagueId'],
                    $flex['summonerId'],
                    $flex['summonerName'],
                    $flex['queueType'],
                    $flex['tier'],
                    $flex['rank'],
                    $flex['leaguePoints'],
                    $flex['wins'],
                    $flex['losses'],
                    $flex['hotStreak'],
                    $flex['veteran'],
                    $flex['freshBlood'],
                    $flex['inactive'],
                    null
                );
        }
        else{
            $flexObj = new LeagueEntryDTO(
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null
            );
        }


        if ($solo){
            $soloObj =  new LeagueEntryDTO(
                $solo['leagueId'],
                $solo['summonerId'],
                $solo['summonerName'],
                $solo['queueType'],
                $solo['tier'],
                $solo['rank'],
                $solo['leaguePoints'],
                $solo['wins'],
                $solo['losses'],
                $solo['hotStreak'],
                $solo['veteran'],
                $solo['freshBlood'],
                $solo['inactive'],
                null
            );
        }else{
            $soloObj = new LeagueEntryDTO(
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null
            );
        }

        return new SummonerInfo($flexObj, $soloObj);

    }
}
