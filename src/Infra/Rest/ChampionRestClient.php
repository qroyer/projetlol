<?php

namespace App\Infra\Rest;

use App\Infra\Dto\Champion;
use Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\DecodingExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;

/**
 * Class ChampionRestClient
 *
 * @package App\Infra\Rest
 */
class ChampionRestClient extends RestClient
{
    const CHAMPION_URI_API = '/lol/platform/v3/champion-rotations';
    const DDRAGON_URL = 'https://ddragon.leagueoflegends.com';

    /**
     * @var string
     */
    private $url;
    /**
     * @var array
     */
    private $freeChampions;

    /**
     * ChampionRestClient constructor.
     */
    public function __construct()
    {
        parent::__construct();
        $this->initFreeChampions();
    }

    /**
     * @return array
     *
     * @throws ClientExceptionInterface
     * @throws DecodingExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws ServerExceptionInterface
     * @throws TransportExceptionInterface
     */
    public function getChampionList()
    {
        $response = $this->httpClient->request(
            'GET',
            self::DDRAGON_URL . '/cdn/10.10.3208608/data/en_US/champion.json'
        );
        $champions = $response->toArray()['data'];

        return $this->getChampionFromArray($champions);
    }

    /**
     * @param $name
     * @return Champion
     * @throws ClientExceptionInterface
     * @throws DecodingExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws ServerExceptionInterface
     * @throws TransportExceptionInterface
     */
    public function getChampionByName($name)
    {
        $response = $this->httpClient->request(
            'GET',
            self::DDRAGON_URL . '/cdn/10.10.3208608/data/en_US/champion/' . $name . '.json'
        );
        $championData = $response->toArray();
        $data = $championData['data'][$name];
        $data['version'] = $championData['version'];
        return $this->createChampionObject($data);
    }

    /**
     * @param $championArray
     * @return array
     */
    private function getChampionFromArray($championArray)
    {
        $championList = array();
        foreach ($championArray as $champion) {
            $championList[$champion['id']] = $this->createChampionObject($champion);
        }

        return $championList;
    }

    /**
     * @param $data
     * @return Champion
     */
    public function createChampionObject($data)
    {
        return new Champion(
            $data['version'],
            $data['id'],
            $data['key'],
            $data['name'],
            $data['title'],
            $data['blurb'],
            $data['info'],
            self::DDRAGON_URL . '/cdn/' . $data['version'] . '/img/champion/' . $data['image']['full'],
            $data['tags'],
            $data['partype'],
            $data['stats'],
            $this->isFreeChampion($data['key'])
        );
    }

    public function isFreeChampion($key): bool
    {
        return in_array($key, $this->freeChampions);
    }

    /**
     * @return array|mixed
     * @throws ClientExceptionInterface
     * @throws DecodingExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws ServerExceptionInterface
     * @throws TransportExceptionInterface
     */
    public function initFreeChampions()
    {
        if (!$this->freeChampions) {
            $response = $this->httpClient->request('GET', $this->getFreeChampionApiUrl());
            $this->freeChampions = $response->toArray()['freeChampionIds'];
        }

        return $this->freeChampions;
    }

    /**
     * @return string
     */
    public function getFreeChampionApiUrl()
    {
        return self::BASE_URL_API . self::CHAMPION_URI_API . '?' . $this->getApiKeyAsUrlParam();
    }

}
