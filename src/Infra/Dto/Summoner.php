<?php


namespace App\Infra\Dto;

class Summoner{

    private $id;
    private $accountId;
    private $puuid;
    private $name;
    private $profileIconId;
    private $revisionDate;
    private $summonerLevel;

    /**
     * Summoner constructor.
     * @param $id
     * @param $accountId
     * @param $puuid
     * @param $name
     * @param $profileIconId
     * @param $revisionDate
     * @param $summonerLevel
     */
    public function __construct($id, $accountId, $puuid, $name, $profileIconId, $revisionDate, $summonerLevel)
    {
        $this->id = $id;
        $this->accountId = $accountId;
        $this->puuid = $puuid;
        $this->name = $name;
        $this->profileIconId = $profileIconId;
        $this->revisionDate = $revisionDate;
        $this->summonerLevel = $summonerLevel;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id): void
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getAccountId()
    {
        return $this->accountId;
    }

    /**
     * @param mixed $accountId
     */
    public function setAccountId($accountId): void
    {
        $this->accountId = $accountId;
    }

    /**
     * @return mixed
     */
    public function getPuuid()
    {
        return $this->puuid;
    }

    /**
     * @param mixed $puuid
     */
    public function setPuuid($puuid): void
    {
        $this->puuid = $puuid;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     */
    public function setName($name): void
    {
        $this->name = $name;
    }

    /**
     * @return mixed
     */
    public function getProfileIconId()
    {
        return $this->profileIconId;
    }

    /**
     * @param mixed $profileIconId
     */
    public function setProfileIconId($profileIconId): void
    {
        $this->profileIconId = $profileIconId;
    }

    /**
     * @return mixed
     */
    public function getRevisionDate()
    {
        return $this->revisionDate;
    }

    /**
     * @param mixed $revisionDate
     */
    public function setRevisionDate($revisionDate): void
    {
        $this->revisionDate = $revisionDate;
    }

    /**
     * @return mixed
     */
    public function getSummonerLevel()
    {
        return $this->summonerLevel;
    }

    /**
     * @param mixed $summonerLevel
     */
    public function setSummonerLevel($summonerLevel): void
    {
        $this->summonerLevel = $summonerLevel;
    }



}