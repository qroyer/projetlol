<?php

namespace App\Infra\Dto;

class LeagueEntryDTO
{

    private $leagueId;

    private $summonerId;

    private $summonerName;

    private $queueType;

    private $tier;

    private $rank;

    private $leaguePoints;

    private $wins;

    private $losses;

    private $hotStreak;

    private $veteran;

    private $freshBlood;

    private $inactive;

    private $miniSeries;

    /**
     * LeagueEntryDTO constructor.
     * @param $leagueId
     * @param $summonerId
     * @param $summonerName
     * @param $queueType
     * @param $tier
     * @param $rank
     * @param $leaguePoints
     * @param $wins
     * @param $losses
     * @param $hotStreak
     * @param $veteran
     * @param $freshBlood
     * @param $inactive
     * @param $miniSeries
     *
     */
    public function __construct(
        $leagueId,
        $summonerId,
        $summonerName,
        $queueType,
        $tier,
        $rank,
        $leaguePoints,
        $wins,
        $losses,
        $hotStreak,
        $veteran,
        $freshBlood,
        $inactive,
        $miniSeries
    )
    {
        $this->leagueId = $leagueId;
        $this->summonerId = $summonerId;
        $this->summonerName = $summonerName;
        $this->queueType = $queueType;
        $this->tier = $tier;
        $this->rank = $rank;
        $this->leaguePoints = $leaguePoints;
        $this->wins = $wins;
        $this->losses = $losses;
        $this->hotStreak = $hotStreak;
        $this->veteran = $veteran;
        $this->freshBlood = $freshBlood;
        $this->inactive = $inactive;
        $this->miniSeries = $miniSeries;
    }


    public function getLeagueId(): ?string
    {
        return $this->leagueId;
    }

    public function setLeagueId(string $leagueId): self
    {
        $this->leagueId = $leagueId;

        return $this;
    }

    public function getSummonerId(): ?string
    {
        return $this->summonerId;
    }

    public function setSummonerId(string $summonerId): self
    {
        $this->summonerId = $summonerId;

        return $this;
    }

    public function getSummonerName(): ?string
    {
        return $this->summonerName;
    }

    public function setSummonerName(string $summonerName): self
    {
        $this->summonerName = $summonerName;

        return $this;
    }

    public function getQueueType(): ?string
    {
        return $this->queueType;
    }

    public function setQueueType(?string $queueType): self
    {
        $this->queueType = $queueType;

        return $this;
    }

    public function getTier(): ?string
    {
        return $this->tier;
    }

    public function setTier(?string $tier): self
    {
        $this->tier = $tier;

        return $this;
    }

    public function getRank(): ?string
    {
        return $this->rank;
    }

    public function setRank(?string $rank): self
    {
        $this->rank = $rank;

        return $this;
    }

    public function getLeaguePoints(): ?int
    {
        return $this->leaguePoints;
    }

    public function setLeaguePoints(?int $leaguePoints): self
    {
        $this->leaguePoints = $leaguePoints;

        return $this;
    }

    public function getWins(): ?int
    {
        return $this->wins;
    }

    public function setWins(?int $wins): self
    {
        $this->wins = $wins;

        return $this;
    }

    public function getLosses(): ?int
    {
        return $this->losses;
    }

    public function setLosses(?int $losses): self
    {
        $this->losses = $losses;

        return $this;
    }

    public function getHotStreak(): ?bool
    {
        return $this->hotStreak;
    }

    public function setHotStreak(?bool $hotStreak): self
    {
        $this->hotStreak = $hotStreak;

        return $this;
    }

    public function getVeteran(): ?bool
    {
        return $this->veteran;
    }

    public function setVeteran(?bool $veteran): self
    {
        $this->veteran = $veteran;

        return $this;
    }

    public function getFreshBlood(): ?bool
    {
        return $this->freshBlood;
    }

    public function setFreshBlood(?bool $freshBlood): self
    {
        $this->freshBlood = $freshBlood;

        return $this;
    }

    public function getInactive(): ?bool
    {
        return $this->inactive;
    }

    public function setInactive(?bool $inactive): self
    {
        $this->inactive = $inactive;

        return $this;
    }

    public function getMiniSeries()
    {
        return $this->miniSeries;
    }

    public function setMiniSeries($miniSeries): self
    {
        $this->miniSeries = $miniSeries;

        return $this;
    }
}
