<?php

namespace App\Infra\Dto;


class MiniSeriesDTO
{

    private $losses;


    private $progress;


    private $target;


    private $wins;


    /**
     * MiniSeriesDTO constructor.
     * @param $losses
     * @param $progress
     * @param $target
     * @param $wins
     */
    public function __construct( $losses, $progress, $target, $wins)
    {
        $this->losses = $losses;
        $this->progress = $progress;
        $this->target = $target;
        $this->wins = $wins;
    }


    public function getLosses(): ?int
    {
        return $this->losses;
    }

    public function setLosses(?int $losses): self
    {
        $this->losses = $losses;

        return $this;
    }

    public function getProgress(): ?string
    {
        return $this->progress;
    }

    public function setProgress(?string $progress): self
    {
        $this->progress = $progress;

        return $this;
    }

    public function getTarget(): ?int
    {
        return $this->target;
    }

    public function setTarget(?int $target): self
    {
        $this->target = $target;

        return $this;
    }

    public function getWins(): ?int
    {
        return $this->wins;
    }

    public function setWins(?int $wins): self
    {
        $this->wins = $wins;

        return $this;
    }
}
