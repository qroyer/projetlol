<?php

namespace App\Infra\Dto;

use App\Infra\Dto\LeagueEntryDTO;

class SummonerInfo{

    /**
     * @var \App\Infra\Dto\LeagueEntryDTO
     */
    private $flex;

    /**
     * @var \App\Infra\Dto\LeagueEntryDTO
     */
    private $solo;

    /**
     * SummonerInfo constructor.
     * @param \App\Infra\Dto\LeagueEntryDTO $flex
     * @param \App\Infra\Dto\LeagueEntryDTO $solo
     */
    public function __construct(LeagueEntryDTO $flex,LeagueEntryDTO $solo)
    {
        $this->flex = $flex;
        $this->solo = $solo;
    }

    /**
     * @return \App\Infra\Dto\LeagueEntryDTO
     */
    public function getFlex(): \App\Infra\Dto\LeagueEntryDTO
    {
        return $this->flex;
    }

    /**
     * @param \App\Infra\Dto\LeagueEntryDTO $flex
     */
    public function setFlex(\App\Infra\Dto\LeagueEntryDTO $flex): void
    {
        $this->flex = $flex;
    }

    /**
     * @return \App\Infra\Dto\LeagueEntryDTO
     */
    public function getSolo(): \App\Infra\Dto\LeagueEntryDTO
    {
        return $this->solo;
    }

    /**
     * @param \App\Infra\Dto\LeagueEntryDTO $solo
     */
    public function setSolo(\App\Infra\Dto\LeagueEntryDTO $solo): void
    {
        $this->solo = $solo;
    }


}