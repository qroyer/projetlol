<?php


namespace App\Infra\Dto;


class DDragonResponse
{
    private $type;
    private $format;
    private $version;
    private $data;
}