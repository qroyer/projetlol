<?php
namespace App\Service;

use App\Infra\Rest\ChampionRestClient;

/**
 * Class ChampionService
 *
 * @package App\Service
 */
class ChampionService
{
    /**
     * @var ChampionRestClient
     */
    private $championRestClient;

    /**
     * ChampionService constructor.
     */
    public function __construct() {
        $this->championRestClient = new ChampionRestClient();
    }

    public function getChampionList()
    {
        return $this->championRestClient->getChampionList();
    }

    public function getChampionByName($name)
    {
        return $this->championRestClient->getChampionByName($name);
    }
}
