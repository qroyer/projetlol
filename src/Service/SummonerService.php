<?php
namespace App\Service;

use App\Infra\Rest\ChampionRestClient;
use App\Infra\Rest\SummonerRestClient;

/**
 * Class ChampionService
 *
 * @package App\Service
 */
class SummonerService
{
    /**
     * @var SummonerRestClient
     */
    private $SummonerRestClient;

    /**
     * SummonerService constructor.
     */
    public function __construct() {
        $this->SummonerRestClient = new SummonerRestClient();
    }

    public function getSummonerByName($name)
    {
        return $this->SummonerRestClient->getSummonerByName($name);
    }

    public function getSummonerInfo($id)
    {
        return $this->SummonerRestClient->getSummonerInfo($id);
    }

    public function getSummonerById($id)
    {
        return $this->SummonerRestClient->getSummonerById($id);
    }

}
