<?php
namespace App\Controller;

use App\Service\ChampionService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
// Include paginator interface
use Knp\Component\Pager\PaginatorInterface;

/**
 * Class ChampionListController
 *
 * @package App\Controller
 */
class ChampionListController extends AbstractController
{
    const LIMIT_PAGINATOR = 24;

    /**
     * @Route("/champion/list", name="champion_list")
     *
     * @param Request $request
     * @param ChampionService $championService
     * @param PaginatorInterface $paginator
     * @return Response
     */
    public function index(
        Request $request,
        ChampionService $championService,
        PaginatorInterface $paginator
    ) {
        $champions = $championService->getChampionList();
        $championsPaginated = $paginator->paginate($champions, $request->query->getInt('page', 1, 10), self::LIMIT_PAGINATOR);

        return $this->render(
            'Champions/champions-list.html.twig', [
                'champions_paginated' => $championsPaginated,
            ]);

    }
}
