<?php
namespace App\Controller;

use App\Service\SummonerService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Security;

/**
 * Class Homepage
 *
 * @package App\Controller
 */
class MyAccountController extends AbstractController
{

    /**
     * @Route("/account", name="my_account")
     *
     * @param SummonerService $summonerService
     * @return Response
     */
    public function index(
        Security $security,
        SummonerService $summonerService
    ) {
        $user = $security->getUser();
        $summonerData = [];

        if($user and $user->getSummonerId())
        {
            $summonerData = $summonerService->getSummonerInfo($user->getSummonerId());
        }
        return $this->render('account/my_account.html.twig',
            ['summonerData' => $summonerData ]);
    }
}
