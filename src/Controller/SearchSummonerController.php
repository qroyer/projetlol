<?php
namespace App\Controller;

use App\Form\SearchSummonerForm;
use App\Service\SummonerService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class SearchSummonerController
 *
 * @package App\Controller
 */
class SearchSummonerController extends AbstractController
{
    /**
     * @Route("/summoner/search", name="summoner_search")
     *
     * @param Request $request
     * @param SummonerService $summonerService
     * @return Response
     */
    public function index(
        Request $request,
        SummonerService $summonerService
    ) {
        $summoner = null;
        $summonerData = null;
        if ($request->getMethod() == "POST" && $searched = $request->request->get('summoner-search')) {
            $summonerData = $this->getSummoner($summonerService, $searched);
            if($summonerData){
                $summoner = $summonerService->getSummonerInfo($summonerData->getId());
            }
        }

        $searchForm = $this->createForm(SearchSummonerForm::class);
        if ($searchForm->handleRequest($request)->isSubmitted() && $searchForm->isValid()) {
            $summonerData = $this->getSummoner($summonerService, $searchForm->getData()['summoner-name']);
            if($summonerData){
                $summoner = $summonerService->getSummonerInfo($summonerData->getId());
            }
        }
        return $this->render('search/summoner_search.html.twig', [
            'search_form' => $searchForm->createView(),
            'summoner' => $summoner
        ]);
    }

    private function getSummoner(SummonerService $summonerService, $value)
    {
        return $summonerService->getSummonerByName($value);
    }
}
