<?php
namespace App\Controller;

use App\Service\SummonerService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class Homepage
 *
 * @package App\Controller
 */
class AjaxSummonerName extends AbstractController
{

    /**
     * @Route("/ajax_summoner_name", name="ajax_summoner_name")
     *
     * @param Request $request
     * @param SummonerService $summonerService
     * @return Response
     */
    public function index(
        Request $request,
        SummonerService $summonerService
    ) {
        $summonerName = $request->get('summoner_name');
        $summoner = $summonerService->getSummonerByName($summonerName);
        $data = ['error' => 1];
        if ($summoner && $summoner->getId()) {
            $data = [
                'summoner_id' => $summoner->getId(),
                'error' => 0
            ];
        }

        $response = new JsonResponse();
        $response->setData(['data' => $data]);

        return $response;
    }
}
