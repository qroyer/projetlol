<?php
namespace App\Controller;

use App\Service\ChampionService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class Homepage
 *
 * @package App\Controller
 */
class Homepage extends AbstractController
{
    /**
     * @Route("/", name="homepage")
     *
     * @param ChampionService $championService
     * @return Response
     */
    public function index(
      ChampionService $championService
    ) {
        return $this->render('homepage.html.twig',
            ['champions' => $championService->getChampionList()]);
    }
}
