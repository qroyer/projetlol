<?php

namespace App\Controller;

use App\Service\ChampionService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;


class ChampionController extends AbstractController
{
    /**
     * @Route("/champion/list/{name}", name="champion")
     */
    public function index(ChampionService $championService, $name )
    {

        return $this->render(
            'champion/index.html.twig',
            ['champion' => $championService->getChampionByName($name)]);
    }
}
