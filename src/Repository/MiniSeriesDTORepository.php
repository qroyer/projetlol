<?php

namespace App\Repository;

use App\Entity\MiniSeriesDTO;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method MiniSeriesDTO|null find($id, $lockMode = null, $lockVersion = null)
 * @method MiniSeriesDTO|null findOneBy(array $criteria, array $orderBy = null)
 * @method MiniSeriesDTO[]    findAll()
 * @method MiniSeriesDTO[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class MiniSeriesDTORepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, MiniSeriesDTO::class);
    }

    // /**
    //  * @return MiniSeriesDTO[] Returns an array of MiniSeriesDTO objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('m')
            ->andWhere('m.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('m.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?MiniSeriesDTO
    {
        return $this->createQueryBuilder('m')
            ->andWhere('m.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
