<?php

namespace App\Repository;

use App\Entity\LeagueEntryDTO;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method LeagueEntryDTO|null find($id, $lockMode = null, $lockVersion = null)
 * @method LeagueEntryDTO|null findOneBy(array $criteria, array $orderBy = null)
 * @method LeagueEntryDTO[]    findAll()
 * @method LeagueEntryDTO[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class LeagueEntryDTORepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, LeagueEntryDTO::class);
    }

    // /**
    //  * @return LeagueEntryDTO[] Returns an array of LeagueEntryDTO objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('l')
            ->andWhere('l.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('l.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?LeagueEntryDTO
    {
        return $this->createQueryBuilder('l')
            ->andWhere('l.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
