function debounce(func, wait, immediate) {
    var timeout;
    return function() {
        var context = this, args = arguments;
        var later = function() {
            timeout = null;
            if (!immediate) func.apply(context, args);
        };
        var callNow = immediate && !timeout;
        clearTimeout(timeout);
        timeout = setTimeout(later, wait);
        if (callNow) func.apply(context, args);
    };
}

window.addEventListener("DOMContentLoaded", (event) => {
    let inputAjaxSummonerName = document.getElementById('inputAjaxSummonerName');
    let inputHiddenIdSummoner = document.getElementById('registration_form_summoner_id');
    let buttonSubmit = document.getElementById('submitButton');
    let callAjax = debounce(function() {
        axios.get(
            'ajax_summoner_name',
            {
                params: {
                    summoner_name: inputAjaxSummonerName.value
                }
            }
        ).then(function(response) {
            let data = response.data.data;
            if (data.error) {
                buttonSubmit.disabled = true;
            } else {
                inputHiddenIdSummoner.value = data.summoner_id;
                buttonSubmit.disabled = false;
            }
        })
    }, 1000);

    inputAjaxSummonerName.addEventListener('keyup', callAjax)
});
